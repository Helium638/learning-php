<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cars}}`.
 */
class m201110_123418_create_cars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cars}}', [
            'id' => $this->primaryKey(),
            'model_name' => $this->string()->notNull(),
            'number_wheels' => $this->integer()->notNull(),
            'number_doors' => $this->integer()->notNull(),
            'driver_left' => $this->boolean()->notNull(),
            'color' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cars}}');
    }
}
