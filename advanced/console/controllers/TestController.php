<?php


namespace console\controllers;


use common\models\User;

class TestController extends \yii\console\Controller
{
    public function actionMyTest(){
        $users=User::find()->andWhere(['status' => User::STATUS_ACTIVE])->all();
        if ($users){
            foreach($users as $user){
                echo $user->id;
            }
        }
        echo 'hello users!';
    }
}