<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ContactForm */

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\DetailView;

$this->title = 'Detail View';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-5">
            <?php
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'username',            // title attribute (in plain text)
                    [                      // the owner name of the model
                        'label' => 'Username',
                        'value' => $model->username,
                    ],
                    'created_at:datetime', // creation date formatted as datetime
                ],
            ]);
            ?>

        </div>
    </div>

</div>
