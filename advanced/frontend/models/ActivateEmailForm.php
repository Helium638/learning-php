<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\web\Controller;


/**
 * Activate email address
 */
class ActivateEmailForm extends Model
{
    public $username;
    public $email;
    public $password;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username, email, password are required
            [['username', 'email', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['email', 'email'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }



    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne([
                'email' => $this->email,
                'username' => $this->username,
            ]);
        }

        return $this->_user;
    }

    /**
     * Activate email address makes user active and able to login.
     *
     * @return bool whether the email activation was successful
     */
    public function activate()
    {
        $user = User::findOne([
            'email' => $this->email,
            //'status' => User::STATUS_INACTIVE
        ]);
        if ($user->status !== 10) {
            $user->status = User::STATUS_ACTIVE;
            return $user->save(false) ? $user : null;
        };
        if ($user->status == 10) {
            $this->addError('email','Your email address has been activated before.');
            return false;
        }
    }

}
