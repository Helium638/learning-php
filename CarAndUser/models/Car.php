<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Car
 * @package app\models
 * @property int $id [int]
 * @property string $model [varchar(55)]
 * @property string $color [varchar(255)]
 * @property int $number_wheels [int]
 * @property int $number_doors [int]
 * @property bool $driver_left [tinyint(1)]
 * @property bool $deleted [tinyint(1)]
 * @property string $created_by [varchar(255)]
 */
class Car extends ActiveRecord
{
    public static function tableName()
    {
        return 'car';
    }

    public function rules()
    {
        return [
            [['model', 'color', 'number_wheels', 'number_doors', 'driver_left'], 'required'],
            [['model', 'color'], 'string'],
            [['number_wheels', 'number_doors'], 'integer'],
            [['driver_left', 'deleted'], 'boolean'],
        ];
    }

    public function getCreator()
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by'
        ]);
    }


}

?>