<?php


namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii;

class CarSearchModel extends Car
{
    // add the public attributes that will be used to store the data to be search
    public $model;
    public $color;
    public $number_doors;
    public $number_wheels;
    public $driver_left;

    // now set the rules to make those attributes safe
    public function rules()
    {
        return [
            [['model', 'color', 'number_doors', 'number_wheels', 'driver_left','creator.id'], 'safe'],
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(),[
            'creator.id'
        ]);
    }

    public function search($params, $page)
    {
        if ($page == 'index') {
            $deleted = 0;
        } elseif ($page = 'deleted') {
            $deleted = 1;
        }

        if(yii::$app->user->isGuest){
            $query = Car::find()
                ->andWhere(['deleted'=>$deleted])
                ->andWhere(['IS','created_by',null]);
        } else{
            $query = Car::find()->andWhere(['deleted'=>$deleted, 'created_by'=>yii::$app->user->id]);
        }


        // create ActiveQuery

        // Important: lets join the query with our previously mentioned relations
        // I do not make any other configuration like aliases or whatever, feel free
        // to investigate that your self
        $query->joinWith(['creator']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance
        $dataProvider->sort->attributes['model'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['model' => SORT_ASC],
            'desc' => ['model' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['color'] = [
            'asc' => ['color' => SORT_ASC],
            'desc' => ['color' => SORT_DESC],
        ];
        // No search? Then return data Provider
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        // We have to do some search... Lets do some magic
        $query->andFilterWhere([
            //... other searched attributes here
        ])
            // Here we search the attributes of our relations using our previously configured
            // ones in "TourSearch"
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['=', 'number_wheels', $this->number_wheels])
            ->andFilterWhere(['=', 'number_doors', $this->number_doors])
            ->andFilterWhere(['=', 'driver_left', $this->driver_left]);

        return $dataProvider;
    }
}

