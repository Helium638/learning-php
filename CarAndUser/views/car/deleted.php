<?php
use yii\helpers\html;
/* @var $this yii\web\View */
/* @var $models \app\models\Car[] */


$this->title = 'Cars in the database';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1 style="color: #337ab7;">Deleted cars</h1>

        <p class="lead">View, delete or recover your deleted car.</p>

    </div>


    <div class="body-content">

        <div class="row">
            <?php
            //$dataProvider = new \yii\data\ArrayDataProvider([
            //   'allModels'=>$models,
            //    'key'=>'id',
            //    'pagination'=>[
            //            'pageSize'=>10
            //    ]
            //]);

            echo \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',            // title attribute (in plain text)
                    [
                            'attribute' => 'model',
                            'value' => 'model'
                    ],
                    [
                        'attribute' => 'color',
                        'value' => 'color'
                    ],
                    [
                        'attribute' => 'number_wheels',
                        'value' => 'number_wheels'
                    ],
                    [
                        'attribute' => 'number_doors',
                        'value' => 'number_doors'
                    ],
                    [
                            'attribute'=>'driver_left',
                            'value'=>function($model){
                                /* @var $model \app\models\Car */
                                if($model->driver_left==1){
                                    return Yii::t('app','Left');
                                }
                                return Yii::t('app','Right');
                            }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {delete} {reset} ',
                        'buttons' => [
                                'reset' => function($url, $model, $key) {
                                    return Html::a('Recover', ['recover', 'id'=>$model->id]);
                                },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>


        <div>
            <span style="margin-bottom: 20px;"><?= Html::a('Home', ['car/index'], ['class' => 'btn btn-primary'])?></span>
        </div>

    </div>
</div>
