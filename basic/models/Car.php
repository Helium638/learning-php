<?php
    namespace app\models;

    use yii\db\ActiveRecord;

    /**
     * Class Car
     * @package app\models
     * @property int $id [int]
     * @property string $model [varchar(55)]
     * @property string $color [varchar(255)]
     * @property int $number_wheels [int]
     * @property int $number_doors [int]
     * @property bool $driver_left [tinyint(1)]
     * @property bool $deleted [tinyint(1)]
     */
    class Car extends ActiveRecord
    {

        public function rules(){
            return[
              [['model','color','number_wheels','number_doors','driver_left'], 'required'],
                [['model', 'color'], 'string'],
                [['number_wheels', 'number_doors'], 'integer'],
                [['driver_left', 'deleted'],'boolean'],
            ];
        }

        /**
         * @return \yii\db\ActiveQuery
         */
        public function getModel()
        {
            return $this->hasOne(Model::className(), ['model_name' => 'model']);
        }

        /**
         * @return \yii\db\ActiveQuery
         */
        public function getColor()
        {
            return $this->hasOne(Color::className(), ['color_name' => 'color']);
        }

    }
?>