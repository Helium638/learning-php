<?php

namespace app\controllers;

use app\models\SearchModel;
use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Car;

class CarController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //$cars = Car::find()->all();
        //return $this->render('index', ['models'=> $cars]);

        $page = 'index';

        $searchModel = new SearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $page);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleted()
    {
        // Action for the Deleted button on index page

        //$cars = Car::find()->all();
        //return $this->render('index', ['models'=> $cars]);

        $page = 'deleted';

        $searchModel = new SearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $page);

        return $this->render('deleted', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionUpdate($id){

        $car = Car::findOne(['id'=>$id]);

        $formData = yii::$app->request->post();
        if($car->load($formData)){
            if($car->save()){
                yii::$app->session->setFlash('message','Your car has been added to the database.');
                return $this->redirect(['index']);
            }
            else{
                yii::$app->session->setFlash('error','Something went wrong.');
            }
        }
        return $this->render('update', ['car' => $car]);
    }

    public function actionCreate(){
        $car = new Car();
        $formData = yii::$app->request->post();
        if($car->load($formData)){
            if($car->save()){
                yii::$app->session->setFlash('message','Your car has been added to the database.');
                return $this->redirect(['index']);
            }
            else{
                yii::$app->session->setFlash('error','Something went wrong.');
            }
        }
        return $this->render('create', ['car' => $car]);
    }

    public function actionView($id){
        return $this->render('view', ['id'=>$id]);
    }

    public function actionDelete($id){
        $car = Car::findOne(['id'=>$id]);
        $deleted = $car->deleted;
        if ($deleted==0) {
            $car->deleted = '1';
            $car->save();
        } elseif($deleted ==1) {
            $car->delete();
        }

        return $this->redirect(['index']);
    }

    public function actionRecover($id){
        $car = Car::findOne(['id'=>$id]);
        $car->deleted = '0';
        $car->save();

        return $this->redirect(['index']);
    }



}
