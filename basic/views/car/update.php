<?php
use yii\helpers\html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $car app\models\Car */

$this->title = 'Update';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Add your car to the database</h1>
    </div>

    <div class="body-content">
        <?= $this->render('_form',['car'=>$car], ['view.php' => $this->title]) ?>
    </div>

</div>
