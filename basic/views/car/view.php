<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $car \frontend\models\ContactForm */

use app\models\Car;
use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\DetailView;

$this->title = 'View';
$this->params['breadcrumbs'][] = $this->title;

$car = Car::findOne(['id'=>$id]);
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-lg-5">
            <?php
            echo DetailView::widget([
                'model' => $car,
                'attributes' => [
                    'model',            // title attribute (in plain text)
                    [                      // the owner name of the model
                        'label' => 'Color',
                        'value' => $car->color,
                    ],
                    'number_wheels',
                    'number_doors',
                    [                      // the owner name of the model
                        'label' => 'Driver Seat',
                        'value'=>function($model){
                            /* @var $model \app\models\Car */
                            if($model->driver_left==1){
                                return Yii::t('app','Left');
                            }
                            return Yii::t('app','Right');
                        },
                    ],
                ],
            ]);
            ?>

        </div>
    </div>


    <div class="col-lg-2">
        <!--<a href=<?php echo yii::$app->homeURL; ?> class="btn btn-primary" >Home</a> -->
        <span style="margin-bottom: 20px;"><?= Html::a('Home', ['car/index'], ['class' => 'btn btn-primary'])?></span>
    </div>
</div>
