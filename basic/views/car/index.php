<?php
use yii\helpers\html;
/* @var $this yii\web\View */
/* @var $models \app\models\Car[] */


$this->title = 'Cars in the database';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1 style="color: #337ab7;">Cars in the database</h1>

        <p class="lead">Create, view, update or delete cars in the database.</p>

    </div>

    <div>
        <span style="margin-bottom: 20px;"><?= Html::a('Create', ['car/create'], ['class' => 'btn btn-primary'])?></span>
    </div>


    <div class="body-content">

        <div class="row">
            <?php
            //$dataProvider = new \yii\data\ArrayDataProvider([
            //   'allModels'=>$models,
            //    'key'=>'id',
            //    'pagination'=>[
            //            'pageSize'=>10
            //    ]
            //]);

            echo \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',            // title attribute (in plain text)
                    [
                            'attribute' => 'model',
                            'value' => 'model'
                    ],
                    [
                        'attribute' => 'color',
                        'value' => 'color'
                    ],
                    [
                        'attribute' => 'number_wheels',
                        'value' => 'number_wheels'
                    ],
                    [
                        'attribute' => 'number_doors',
                        'value' => 'number_doors'
                    ],
                    [
                            'attribute'=>'driver_left',
                            'value'=>function($model){
                                /* @var $model \app\models\Car */
                                if($model->driver_left==1){
                                    return Yii::t('app','Left');
                                }
                                return Yii::t('app','Right');
                            }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete} ',
                    ],
                ],
            ]);
            ?>
        </div>

        <div>
            <span style="margin-bottom: 20px;"><?= Html::a('Deleted', ['car/deleted'], ['class' => 'btn btn-primary'])?></span>
        </div>

    </div>
</div>
