<?php

use yii\helpers\html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $car app\models\Car */

$form = ActiveForm::begin()
?>
<div class="row">
    <div class="form-group">
        <div class="col-lg-6">
            <?= $form->field($car, 'model'); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-lg-6">
            <?php $items = ['red' => 'red', 'orange' => 'orange', 'yellow' => 'yellow',
                'green' => 'green', 'blue' => 'blue', 'purple' => 'purple',
                'black' => 'black', 'white' => 'white', 'grey' => 'grey',]; ?>
            <?= $form->field($car, 'color')->dropDownList($items, ['prompt' => 'Choose a color...']); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-lg-6">
            <?= $form->field($car, 'number_wheels')->textInput()->label('number of wheels'); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-lg-6">
            <?= $form->field($car, 'number_doors')->textInput()->label('number of doors'); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-lg-6">
            <?= $form->field($car, 'driver_left')->checkbox([
                'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            ]) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-lg-6">
            <div class="col-lg-3">
                <?php if ($this->title == 'Create') {
                    echo Html::submitButton('Create', ['class' => 'btn btn-primary']);
                } elseif($this->title == 'Update') {
                    echo Html::submitButton('Update', ['class' => 'btn btn-primary']);
                }
                ?>
            </div>
            <div class="col-lg-2">
                <!--<a href=<?php echo yii::$app->homeURL; ?> class="btn btn-primary" >Home</a> -->
                <span style="margin-bottom: 20px;"><?= Html::a('Home', ['car/index'], ['class' => 'btn btn-primary'])?></span>
            </div>
        </div>
    </div>
</div>


<?php
$form = ActiveForm::end()
?>
